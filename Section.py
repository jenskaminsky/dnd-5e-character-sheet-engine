from Container import BBox, HBox, VBox, Overlaying, TextBox, MainCanvas, HStack, VStack, SizedBox, HEqualized, \
  VEqualized, HFillLast, VFillLast
import xml.etree.ElementTree as ET
import json

PRIMARY_COLOR = '#FF0000'
SECONDARY_COLOR = '#00FF00'
TERTIARY_COLOR = '#0000FF'
QUATERNARY_COLOR = '#0F0F0F'

FONT = 'arial.ttf'

# TODO: make editable
with open('character.json', 'r') as f:
  INFO = json.load(f)


class Category:

  def __init__(self):
    self._outer = BBox(bg=PRIMARY_COLOR, padding=3)
    self._border = BBox(bg='black', padding=3)
    self._inner = BBox(bg=SECONDARY_COLOR, padding=3)
    self._outer.set_children(self._border)
    self._border.set_children(self._inner)

  def draw(self, im_draw, x0, y0, x1, y1):
    self._outer.draw(im_draw, x0, y0, x1, y1)

  def set_children(self, *item):
    self._inner.set_children(*item)

  def min_w(self): return self._outer.min_w()

  def min_h(self): return self._outer.min_h()


class Attributes:
  def __init__(self):
    ATTR_ORDER = ['str', 'dex', 'con', 'int', 'wis', 'cha']
    SKILL_GROUPS = [['Athletics'],
                    ['Acrobatics', 'Sleight of Hand', 'Stealth'],
                    [],
                    ['Arcana', 'History', 'Investigation', 'Nature', 'Religion'],
                    ['Animal Handling', 'Insight', 'Medicine', 'Perception', 'Survival'],
                    ['Deception', 'Intimidation', 'Performance', 'Persuasion']]
    ATTRS = [(attr, INFO['attributes'][attr]) for attr in ATTR_ORDER]

    SKILL_GROUPS_MOD = [[(skill, INFO['skills'][skill]) for skill in group] for group in SKILL_GROUPS]

    SKILL_GROUPS_MOD[2].append(('Drowning (min)', '5'))

    inner = ''.join(f'<HFillLast bg="{SECONDARY_COLOR}" hgap="5">' \
                    f'  {self.gen_attribute(attr[0], attr[1]["score"], attr[1]["mod"], attr[1]["save"])}' \
                    f'  {self.gen_skill_container(skill_group)}' \
                    f'</HFillLast>'
                    for attr, skill_group in zip(ATTRS, SKILL_GROUPS_MOD))

    outer = f'<Category>' \
            f'  <VEqualized bg="{PRIMARY_COLOR}" vgap="5">' \
            f'    {inner}' \
            f'  </VEqualized>' \
            f'</Category>'

    print(outer)
    self._outer = generate_from_xml_str(outer)

  def set_children(self, *item):
    pass

  def draw(self, im_draw, x0, y0, x1, y1):
    self._outer.draw(im_draw, x0, y0, x1, y1)

  def min_w(self): return self._outer.min_w()

  def min_h(self): return self._outer.min_h()

  def gen_skill_container(self, skills):
    skills = ''.join(self.gen_skill(name, mod) for name, mod in skills)
    str_rep = f'<VStack bg="{SECONDARY_COLOR}" vgap="5">' \
              f'  {skills}' \
              f'</VStack>'
    return str_rep if skills else ''

  def gen_skill(self, name, modifier):
    str_rep = f'<HFillLast bg="{PRIMARY_COLOR}" hgap="5">' \
              f'  <BBox bg="{TERTIARY_COLOR}" padding="5">' \
              f'    <Overlaying bg="{SECONDARY_COLOR}">' \
              f'      <SizedBox bg="null" width="40" height="40" halign="C" valign="C"/>' \
              f'      <TextBox text="{modifier}" font="{FONT}" size="40" padding="0" halign="C" valign="C"/>' \
              f'    </Overlaying>' \
              f'  </BBox>' \
              f'  <TextBox text="{name}" font="{FONT}" size="30" padding="10" halign="L" valign="C"/>' \
              f'</HFillLast>'
    return str_rep

  def gen_attribute(self, name, score, mod, save):
    str_rep = f'<VStack bg="{SECONDARY_COLOR}" vgap="5">' \
              f'  <Overlaying bg="{TERTIARY_COLOR}">' \
              f'    <TextBox text="{name}" font="{FONT}" size="30" padding="5" halign="L" valign="T"/>' \
              f'    <TextBox text="{mod}" font="{FONT}" size="120" padding="40" halign="C" valign="C"/>' \
              f'  </Overlaying>' \
              f'  <HBox bg="{PRIMARY_COLOR}" hgap="5">' \
              f'    <BBox bg="{TERTIARY_COLOR}" padding="0">' \
              f'      <TextBox text="{score}" font="{FONT}" size="40" padding="10" halign="C" valign="C"/>' \
              f'    </BBox>' \
              f'    <BBox bg="{TERTIARY_COLOR}" padding="0">' \
              f'      <TextBox text="{save}" font="{FONT}" size="40" padding="10" halign="C" valign="C"/>' \
              f'    </BBox>' \
              f'  </HBox>' \
              f'</VStack>'
    return str_rep


class BasicInfo:
  def __init__(self):
    basic_info = INFO['basic_info']

    outer = f'<Category>' \
            f'  <VStack bg="{SECONDARY_COLOR}" vgap="5">' \
            f'    <HFillLast bg="{QUATERNARY_COLOR}" hgap="5">' \
            f'      <TextField title="Level" text="{basic_info["character_level"]}"/>' \
            f'      <TextField title="Name" text="{basic_info["name"]}"/>' \
            f'    </HFillLast>' \
            f'    <HBox bg="{QUATERNARY_COLOR}" hgap="5">' \
            f'      <TextField title="Race" text="{basic_info["race"]}"/>' \
            f'      <TextField title="Background" text="{basic_info["background"]}"/>' \
            f'    </HBox>' \
            f'    {self.gen_classes(basic_info["classes"])}' \
            f'  </VStack>' \
            f'</Category>'

    print(outer)
    self._outer = generate_from_xml_str(outer)

  def set_children(self, *item):
    pass

  def draw(self, im_draw, x0, y0, x1, y1):
    self._outer.draw(im_draw, x0, y0, x1, y1)

  def min_w(self): return self._outer.min_w()

  def min_h(self): return self._outer.min_h()

  def gen_classes(self, classes):
    classes_str = ''.join(f'<HFillLast bg="{PRIMARY_COLOR}" hgap="5">' \
                          f'  <BBox bg="{TERTIARY_COLOR}" padding="5">' \
                          f'    <TextBox text="{c["level"]}" font="{FONT}" size="40" padding="5" halign="L" valign="C"/>' \
                          f'  </BBox>' \
                          f'  <BBox bg="{TERTIARY_COLOR}" padding="5">' \
                          f'    <TextBox text="{c["class"]}" font="{FONT}" size="40" padding="5" halign="L" valign="C"/>' \
                          f'  </BBox>' \
                          f'</HFillLast>'
                          for c in classes)
    str_rep = f'<VEqualized bg="{QUATERNARY_COLOR}" vgap="5">' \
              f'  {classes_str}' \
              f'</VEqualized>'
    return classes_str

class TextField:
  def __init__(self, title, text):
    outer = f'      <Overlaying bg="{TERTIARY_COLOR}">' \
            f'        <TextBox text="{title}" font="{FONT}" size="20" padding="0" halign="L" valign="T"/>' \
            f'        <TextBox text="{text}" font="{FONT}" size="40" padding="20" halign="C" valign="C"/>' \
            f'      </Overlaying>' \

    print(outer)
    self._outer = generate_from_xml_str(outer)

  def set_children(self, *item):
    pass

  def draw(self, im_draw, x0, y0, x1, y1):
    self._outer.draw(im_draw, x0, y0, x1, y1)

  def min_w(self): return self._outer.min_w()

  def min_h(self): return self._outer.min_h()

#
# HELPER
#
def generate_from_xml_file(file_name):
  tree = ET.parse(file_name)
  root = obj_from_xml(tree.getroot())
  return root


def generate_from_xml_str(text):
  tree = ET.fromstringlist(text)
  root = obj_from_xml(tree)
  return root


CONTAINERS = {'Overlaying': Overlaying,
              'TextBox': TextBox,
              'MainCanvas': MainCanvas,
              'HBox': HBox,
              'VBox': VBox,
              'BBox': BBox,
              'HStack': HStack,
              'VStack': VStack,
              'SizedBox': SizedBox,
              'HEqualized': HEqualized,
              'VEqualized': VEqualized,
              'HFillLast': HFillLast,
              'VFillLast': VFillLast,

              'Category': Category,
              'TextField': TextField,

              'Attributes': Attributes,
              'BasicInfo': BasicInfo}

NON_STR_START = ['[', '{'] + list(str(i) for i in range(10))


def obj_from_xml(xml_node):
  tag = xml_node.tag
  attributes = {k: json.loads(v) if v[0] in NON_STR_START else v
                for k, v in xml_node.attrib.items()}
  cont = CONTAINERS[tag](**attributes)
  children = [obj_from_xml(c) for c in xml_node.getchildren()]
  cont.set_children(*children)
  return cont
