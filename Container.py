from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import matplotlib.pyplot as plt


class Cont:
  def __init__(self, bg):
    self._bg = bg

  def _draw(self, im_draw, x0, y0, x1, y1):
    if self._bg != 'null':
      im_draw.rectangle(xy=[x0, y0, x1, y1], fill=self._bg)


class OneCont(Cont):
  def __init__(self, bg):
    super().__init__(bg)
    self._child = None

  def set_children(self, *item):
    if item: self._child = item[0]


class MultiCont(Cont):
  def __init__(self, bg):
    super().__init__(bg)
    self._children = []

  def set_children(self, *item):
    self._children = []
    self._children.extend(item)


#
# specific
#
class Overlaying(MultiCont):
  def __init__(self, bg):
    super().__init__(bg)

  def draw(self, im_draw, x0, y0, x1, y1):
    self._draw(im_draw, x0, y0, x1, y1)
    for child in self._children:
      child.draw(im_draw, x0, y0, x1, y1)

  def min_w(self): return max(c.min_w() for c in self._children) if self._children else 0

  def min_h(self): return max(c.min_h() for c in self._children) if self._children else 0


class TextBox:
  def __init__(self, text, font, size, padding, halign='C', valign='C'):
    self._text = str(text)
    self._padding = padding
    self._halign = halign
    self._valign = valign
    self._label = ImageFont.truetype(font, size)
    self._width, self._height = self._label.getsize(self._text)

  def draw(self, im_draw, x0, y0, x1, y1):
    assert x1 - x0 >= self._width and y1 - y0 >= self._height
    x = {'L': x0 + self._padding,
         'C': x0 + (x1 - x0) / 2 - self._width / 2,
         'R': x1 - self._padding - self._width}
    y = {'T': y0 + self._padding,
         'C': y0 + (y1 - y0) / 2 - self._height / 2,
         'B': y1 - self._padding - self._height}
    im_draw.text(xy=(x[self._halign], y[self._valign]),
                 text=self._text, font=self._label, fill='black')

  def set_children(self, *item):
    pass

  def min_w(self): return self._width + 2 * self._padding

  def min_h(self): return self._height + 2 * self._padding


class MainCanvas(OneCont):
  def __init__(self, mode, size, bg):
    super().__init__(bg)
    self._mode = mode
    self._size = size

  def show(self):
    im = Image.new(mode=self._mode, size=self._size, color=self._bg)
    im_draw = ImageDraw.Draw(im)
    x1, y1 = self._size
    self.draw(im_draw, 0, 0, x1, y1)
    plt.imshow(im)
    plt.show()

  def save(self, path):
    im = Image.new(mode=self._mode, size=self._size, color=self._bg)
    im_draw = ImageDraw.Draw(im)
    x1, y1 = self._size
    self.draw(im_draw, 0, 0, x1, y1)
    im.save(path)

  def draw(self, im_draw, x0, y0, x1, y1):
    if self._child:
      self._child.draw(im_draw, 0, 0, x1, y1)


class HBox(MultiCont):
  def __init__(self, bg, hgap):
    super().__init__(bg)
    self._hgap = hgap

  def draw(self, im_draw, x0, y0, x1, y1):
    self._draw(im_draw, x0, y0, x1, y1)

    n = len(self._children)
    for idx, child in enumerate(self._children):
      cW = x1 - x0
      iW = (cW - (n - 1) * self._hgap) / n
      child.draw(im_draw,
                 x0=x0 + idx * iW + idx * self._hgap,
                 y0=y0,
                 x1=x0 + (idx + 1) * iW + idx * self._hgap,
                 y1=y1)

  def min_w(self): return max(c.min_w() for c in self._children) * len(self._children) + \
                          sum(self._hgap for _ in self._children[1:])

  def min_h(self): return max(c.min_h() for c in self._children)


class VBox(MultiCont):
  def __init__(self, bg, vgap):
    super().__init__(bg)
    self._vgap = vgap

  def draw(self, im_draw, x0, y0, x1, y1):
    self._draw(im_draw, x0, y0, x1, y1)

    n = len(self._children)
    cH = y1 - y0
    iH = (cH - (n - 1) * self._vgap) / n
    for idx, child in enumerate(self._children):
      child.draw(im_draw,
                 x0=x0,
                 y0=y0 + idx * iH + idx * self._vgap,
                 x1=x1,
                 y1=y0 + (idx + 1) * iH + idx * self._vgap)

  def min_w(self): return max(c.min_w() for c in self._children)

  def min_h(self): return max(c.min_h() for c in self._children) * len(self._children) + \
                          sum(self._vgap for _ in self._children[1:])


class BBox(OneCont):
  def __init__(self, bg, padding):
    super().__init__(bg)
    self._padding = padding

  def draw(self, im_draw, x0, y0, x1, y1):
    self._draw(im_draw, x0, y0, x1, y1)
    if self._child:
      self._child.draw(im_draw,
                       x0=x0 + self._padding,
                       y0=y0 + self._padding,
                       x1=x1 - self._padding,
                       y1=y1 - self._padding)

  def min_w(self): return (2 * self._padding + self._child.min_w()) if self._child else 0

  def min_h(self): return (2 * self._padding + self._child.min_h()) if self._child else 0


class HStack(MultiCont):
  def __init__(self, bg, hgap):
    super().__init__(bg)
    self._hgap = hgap

  def draw(self, im_draw, x0, y0, x1, y1):
    self._draw(im_draw, x0, y0, x1, y1)
    for child in self._children:
      x1 = x0 + child.min_w()
      child.draw(im_draw,
                 x0=x0,
                 y0=y0,
                 x1=x1,
                 y1=y1)
      x0 = x1 + self._hgap

  def min_w(self): return sum(c.min_w() for c in self._children) + sum(self._hgap for _ in self._children[1:])

  def min_h(self): return max(c.min_h() for c in self._children)


class VStack(MultiCont):
  def __init__(self, bg, vgap):
    super().__init__(bg)
    self._vgap = vgap

  def draw(self, im_draw, x0, y0, x1, y1):
    self._draw(im_draw, x0, y0, x1, y1)
    for child in self._children:
      y1 = y0 + child.min_h()
      child.draw(im_draw,
                 x0=x0,
                 y0=y0,
                 x1=x1,
                 y1=y1)
      y0 = y1 + self._vgap

  def min_w(self): return max(c.min_w() for c in self._children)

  def min_h(self): return sum(c.min_h() for c in self._children) + sum(self._vgap for _ in self._children[1:])


class SizedBox(Cont):
  def __init__(self, bg, width, height, halign='C', valign='C'):
    super().__init__(bg)
    self._halign = halign
    self._valign = valign
    self._width = width
    self._height = height

  def draw(self, im_draw, x0, y0, x1, y1):
    x = {'L': x0,
         'C': x0 + (x1 - x0) / 2 - self._width / 2,
         'R': x1 - self._width}
    y = {'T': y0,
         'C': y0 + (y1 - y0) / 2 - self._height / 2,
         'B': y1 - self._height}
    self._draw(im_draw, x[self._halign], y[self._valign], x[self._halign] + self._width,
               y[self._valign] + self._height)

  def set_children(self, *item):
    pass

  def min_w(self): return self._width

  def min_h(self): return self._height


class HEqualized(MultiCont):
  def __init__(self, bg, hgap):
    super().__init__(bg)
    self._hgap = hgap

  def draw(self, im_draw, x0, y0, x1, y1):
    self._draw(im_draw, x0, y0, x1, y1)
    max_w = max(c.min_w() for c in self._children)
    for child in self._children:
      x1 = x0 + max_w
      child.draw(im_draw,
                 x0=x0,
                 y0=y0,
                 x1=x1,
                 y1=y1)
      x0 = x1 + self._hgap

  def min_w(self):
    return len(self._children) * max(c.min_w() for c in self._children) + sum(self._hgap for _ in self._children[1:])

  def min_h(self):
    return max(c.min_h() for c in self._children)


class VEqualized(MultiCont):
  def __init__(self, bg, vgap):
    super().__init__(bg)
    self._vgap = vgap

  def draw(self, im_draw, x0, y0, x1, y1):
    self._draw(im_draw, x0, y0, x1, y1)
    max_h = max(c.min_h() for c in self._children)
    for child in self._children:
      y1 = y0 + max_h
      child.draw(im_draw,
                 x0=x0,
                 y0=y0,
                 x1=x1,
                 y1=y1)
      y0 = y1 + self._vgap

  def min_w(self):
    return max(c.min_w() for c in self._children)

  def min_h(self):
    return len(self._children) * max(c.min_h() for c in self._children) + sum(self._vgap for _ in self._children[1:])


class HFillLast(MultiCont):
  def __init__(self, bg, hgap):
    super().__init__(bg)
    self._hgap = hgap

  def draw(self, im_draw, x0, y0, x1, y1):
    self._draw(im_draw, x0, y0, x1, y1)

    for child in self._children[:-1]:
      x2 = x0 + child.min_w()
      child.draw(im_draw,
                 x0=x0,
                 y0=y0,
                 x1=x2,
                 y1=y1)
      x0 = x2 + self._hgap

    for child in self._children[-1:]:
      child.draw(im_draw,
                 x0=x0,
                 y0=y0,
                 x1=x1,
                 y1=y1)


  def min_w(self): return sum(c.min_w() for c in self._children) + sum(self._hgap for _ in self._children[1:])

  def min_h(self): return max(c.min_h() for c in self._children)


class VFillLast(MultiCont):
  def __init__(self, bg, vgap):
    super().__init__(bg)
    self._vgap = vgap

  def draw(self, im_draw, x0, y0, x1, y1):
    self._draw(im_draw, x0, y0, x1, y1)

    for child in self._children[:-1]:
      y2 = y0 + child.min_h()
      child.draw(im_draw,
                 x0=x0,
                 y0=y0,
                 x1=x1,
                 y1=y2)
      y0 = y2 + self._vgap

    for child in self._children[-1:]:
      child.draw(im_draw,
                 x0=x0,
                 y0=y0,
                 x1=x1,
                 y1=y1)


  def min_w(self): return max(c.min_w() for c in self._children)

  def min_h(self): return sum(c.min_h() for c in self._children) + sum(self._vgap for _ in self._children[1:])
